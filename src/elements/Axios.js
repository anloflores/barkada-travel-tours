import axios from 'axios';

axios.defaults.baseURL = 'http://complexus.xyz/';

export default axios;