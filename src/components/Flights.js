import React from 'react';
import moment from 'moment';

import { connect } from "react-redux";
import { saveOnwardFlight, saveReturnFlight, setCurrentResults } from "../redux/actions/actions";

function Flights(props) {
    return (
      <div className="flight-item">
        <div className="container">
          
          {
            props.flight.flights.map((flight, i) => {
              return (
                <div>
                  <div className="row">
                    <div className="col-12 flight-icons">
                      <img
                        className="float-left"
                        src={"http://btt.complexus.tech/images/airlines/"+ flight.carrier.code +".gif"}
                        alt=""
                      />
                      <h1 className="title airline float-left">
                        <div>{ flight.carrier.name }</div>
                        <small className="small">{ props.flight.flightKey }</small>
                      </h1>

                      {/* <div className="float-right">
                        <i className="fa fa-fw fa-2x question fa-question-circle"> </i>
                      </div> */}
                      <div className="clearfix"></div>
                    </div>
                  </div>
                  <br />
                  <hr />
                  <br />
                  <div className="row">
                    <div className="col-4 text-right">
                      <h1 className="title text-right">
                        <div>{ moment(flight.depDetail.time).format('HH:mm') }</div>
                        <small>{ flight.depDetail.code }</small>
                      </h1>
                    </div>
                    <div className="col-4 text-center">
                      <ul>
                        <li>{ Math.floor(flight.flyTime/60) }h { Math.floor(flight.flyTime%60) }</li>
                      </ul>

                      { flight.stops != 0 ? <small>{flight.stops} STOP</small> : '' }
                    </div>
                    <div className="col-4">
                      <h1 className="title">
                        <div>{ moment(flight.arrDetail.time).format('HH:mm') } { flight.nextDatArr ? <sup>+1</sup> : '' }</div>
                        <small>{ flight.arrDetail.code }</small>
                      </h1>
                    </div>
                  </div>
                  <br />
                  <hr />
                </div>
              );
            })
          }

          <div className="row flight-icons">
            <div className="col-12">
              <button className="btn btn-primary btn-sm float-right" onClick={() => {
                if(props.current == 'onwardJourneys') {
                  alert('onward');
                  setCurrentResults("returnJourneys");
                  saveOnwardFlight(props.flight);
                } else {
                  alert("return")
                  saveReturnFlight(props.flight)
                }
              }}>
                Select
              </button>
              <h1 className="title float-left">
                ₱{props.flight.fares.totalFare.total.amount}
              </h1>
              <div className="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    );
}

const mapStateToProps = state => {
  return {
    onwardFlight: state.onwardFlight,
    returnFlight: state.returnFlight,
    currentResults: state.currentResults
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveOnwardFlight: onwardFlight => dispatch(saveOnwardFlight(onwardFlight)),
    saveReturnFlight: returnFlight => dispatch(saveReturnFlight(returnFlight)),
    setCurrentResults: currentResults => dispatch(setCurrentResults(currentResults))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Flights);

