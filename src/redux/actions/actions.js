import axios from 'axios'

// export const co_url = 'https://production.complexus.xyz/';
export const co_url = 'http://127.0.0.1:8000/';
export const pk_url = 'https://pitakamo.cboxpay.com:7443/pitakamo/webapi/partner/bakarda/';
// export const pk_url = "https://pitakamo.com:8443/pitakamo/webapi/partner/bakarda/";
export const pk_token = "?access_token=0739a47f-7a22-4a2a-8680-875e8242d7db";

export const saveToken = () => {
    return (dispatch) => {
        var data = {
          grant_type: "password",
          client_id: "2",
          client_secret: "mTx66iKXlQ5AkbitrGmGshe3ybXTSfLztriIpgjV",
          username: "test@gmail.com",
          password: "password",
          scope: "can-search-flights"
        };

        return axios({
            url: co_url + "oauth/token",
            method: 'post',
            data: data,
            headers: {
              'Content-Type': 'application/json'
            }
        })
        .then(response => {
            dispatch({
                type: 'SAVE_TOKEN',
                token: response.data
            })
        });
    }
}

export const saveSearch = (search) => {
    return (dispatch) => {
        dispatch({
          type: "SAVE_SEARCH",
          search: search
        });
    }
}

export const saveOnwardFlight = (onwardFlight) => {
    return (dispatch) => {
        dispatch({
          type: "SAVE_ONWARD",
          onwardFlight: onwardFlight
        });
    }
}

export const saveReturnFlight = (returnFlight) => {
    return (dispatch) => {
        dispatch({
          type: "SAVE_RETURN",
          returnFlight: returnFlight
        });
    }
}

export const setCurrentResults = (currentResults) => {
    return (dispatch) => {
        dispatch({
          type: "SET_CURRENT_RESULTS",
          currentResults: currentResults
        });
    }
}

export const saveResult = (searchData) => {
    return (dispatch, getState) => {
        return axios({
          url: co_url + "api/flights/search",
          method: "get",
          params: searchData,
          headers: {
            Authorization: "Bearer " + getState().token.access_token
          }
        }).then(response => {
          dispatch({
            type: "SAVE_RESULTS",
            results: response.data
          });
        });
    }
}

export const saveKYC = (id) => {
    return (dispatch, getState) => {
        return axios({
          url: pk_url + "preview_account_info" + pk_token,
          method: "post",
          data: {
            'P_PITAKAMO_ACCOUNT': id
          },
          headers : {
              'Content-Type' : 'application/json'
          }
        }).then(response => {
          if(response.data.status =="Error") return false;

          dispatch({
            type: "SAVE_KYC",
            kyc: response.data.data
          });
          
          return true;
        });
    }
}

export const calculateFee = (params) => {
  return (dispatch, getState) => {
    return axios({
      url: pk_url + "calculate_transaction_fee" + pk_token,
      method: "post",
      data: params,
      headers: {
        'Content-Type' : 'application/json'
      }
    }).then(response=> {
      if(response.data.status == "Error") return false;
      dispatch({
        type: "CALCULATE_FEE",
        fees: response.data.data
      })
      return response.data.data;
    })
  }
}

export const repriceFlight = () => {
  return (dispatch, getState) => {
    return axios({
      url: co_url + "api/flights/review",
      method: "post",
      data: {
        keys: getState().flightKeys
      },
      headers: {
        Authorization: "Bearer " + getState().token.access_token
      }
    }).then(response => {
      dispatch({
        type: "SAVE_REPRICE",
        reprice: response.data
      });
    });
  }
}

export const setFlightKeys = (keys) => {
  return (dispatch, getState) => {
    dispatch({
      type: "SET_FLIGHT_KEYS",
      keys: keys
    })
  }
}

export const setPassengerData  = (passengersData) => {
  return (dispatch, getState) => {
    dispatch({
      type: "SET_PASSENGER_DATA",
      passengersData: passengersData
    });
  }
}

export const setTotalMeal  = (totalMeal) => {
  return (dispatch, getState) => {
    dispatch({
      type: "SET_TOTAL_MEAL",
      totalMeal: totalMeal
    });
  }
}

export const setTotalBaggage  = (totalBaggage) => {
  return (dispatch, getState) => {
    dispatch({
      type: "SET_TOTAL_BAGGAGE",
      totalBaggage: totalBaggage
    });
  }
}

export const makePitakaMoPayment = (params) => {
  return (dispatch, getState) => {
        return axios({
          url: pk_url + "request_payment_Transfer" + pk_token,
          method: "post",
          data: params,
          headers: {
            "Content-Type": "application/json"
          }
        }).then(response => {
          if (response.data.status == "Error") return response.data.userMessage;

          dispatch({
            type: "MAKE_PITAKAMO_PAYMENT",
            kyc: response.data.data
          });

          return true;
        });
    }
}

export const makeBooking = (totalPrice) => {
  return (dispatch, getState) => {
    return axios({
      url: co_url + "api/flights/book",
      method: "post",
      data: {
        dd: {
          mobile: getState().kyc[0].KYC_MOBILE, // Number Client
          isdCode: "63",
          email: getState().kyc[0].EMAIL // Barkada
        },
        ik: getState().reprice.itinKey,
        pd: getState().passengersData,
        tl: totalPrice,
        keys: getState().flightKeys
      },
      headers: {
        Authorization: "Bearer " + getState().token.access_token
      }
    }).then(response => {
      if(response.data.refId === null) return false;

      dispatch({
        type: "BOOK_FLIGHT",
        booking: response.data
      });

      return true;
    });
  };
}