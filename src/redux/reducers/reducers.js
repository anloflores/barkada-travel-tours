const initialState = {
  // token: {
  //   token_type: "Bearer",
  //   expires_in: 31622400,
  //   access_token:
  //     "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNDczMDFjNGJlOTk3OWViZGZmYTQ0MDJhODhhOTQ1YjI1N2JjNmFiMjYwOTU4YTM5YTY0M2QwZGJhZmVjMzBiOTc2NDcyNjRjZWE1OWFlYzAiLCJpYXQiOjE1ODI1NTcxNjksIm5iZiI6MTU4MjU1NzE2OSwiZXhwIjoxNjE0MTc5NTY5LCJzdWIiOiIxIiwic2NvcGVzIjpbIioiXX0.lJpMetvr_GdU8oNuwPV9q23h4allKaGqgspCXafgQzKGNYpOhx9Z_bKCvB8r2tHDNSM_hEv0UiVUj7Hk1ednAbpOpZiL1LApnLbpJ_M8eoO2cBghQOhn_9UuNKND6EeEZ_3JZSbfyQYpeZo79QOru_8ijkyJ3OiObN8_WWsG3to24itUqqQIwO3moPfC19C0rU4vbvFoal1HT4LNUTS4d5CSzAEMjWMrlBQV-Reqofg31ERdMhDEJu3vvTQ-K7lTJBrvrSJErXT55AGhJk3k0OKidsDcyG7rhcvQzv4g4yHkEyq2-Lv4qULf0f0AYUX__JhJjxPVFohmhKlSvDFuf9JBGXqQJA5UKCeVDG-vdK6zPCq5LT_Xfba1yuI7I-Cnp4um_FMiSw_Y2NxEj6df1jCwo_t3HsqjMNgv0xMs12Y7IzfwMhdj6vl5insLYaHIogWNnF1MXO4KVwxEg-Ut8kMYlUWvrdmBWCdz-rN8vRKUlbJUkl5tLbKeyUrRaBlmfGJrwX4zZAMDVdi8FNzDMksq8nvvRPdhc8-5tFzVwqLC7cxAVLstZPGAMc5HyG4NjJydizoY1dHXB_NJ1iTZeNmf52GNjlx56ZP4iWq1OGZLdrD75WLVUxMY3Di6SFhvG0GLs2yEg7NDSJWp1Thect-VKNbsI-I8LxXgo5HZl4I",
  //  },
  token: {
    token_type: "Bearer",
    expires_in: 31622400,
    access_token:
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiMmQyOGZiM2UyZGQ1ZGI2NTk2NjMyNjM1Yzc1MTgzYmRlNTBiOTUzMjE2MzQ1NThiNmE0ZTRjYmJiYjA3MjA0OGQxYmQ0ZDQyNjY0NjlhMDIiLCJpYXQiOjE1Nzk5NzEwNTIsIm5iZiI6MTU3OTk3MTA1MiwiZXhwIjoxNjExNTkzNDUyLCJzdWIiOiIxIiwic2NvcGVzIjpbIioiXX0.WaIkTFtog8yWL3tWCcLd7oOJEtoUK9TRgLq7FsszvrTblvXceinyL_-4QUXwmBoD2qjvbIPnsLEOWWoJBQoi2JzrpZkQ9ZKSvoxwMD4etIBFE7MaGo8P6P_FDCkpOIPuVwKWrolEP8H8oj9dRwLaHFXhNOrs8Fr2zow1yb9lMCVcGFJEe24fIFAstSegXVHh72IEyeePI6TsRYU-NFWvXp2_pJRzj_sIHVsmyeTRqdCN0rGbw4FueOqgrXESvYnPolFZIEXusByZgsOfRJCQpFFkH-Hhcsbwv-6aHoRCnyTp7YXYuLZfj8LB0JCfZIR4TRlnIuEywi-1zu57Djrq6JgTIUVArgMGD3-CxslqT-Ce839KCbTKGb83rRs2RZZJO5CrhRH7KwjEQBt26MKx6TXrEg0ElShZ42bEc0lMEEBE3EZp_udD_OLDfgjY5CV0xbUai8CzbA0c-KD3g-txZOInjKd5tmhFVdE5YbGLYvf6FHq_auI2QvPwFA5aaka39sVlV5ui_pFE5tDVB74yzdkQquJT-Af1s4eDTjcSQ-UIb2JH8dqPvo6Erq2ORMgY-JGhzjyckw8uDbJKFE08DW09fRwmzm2qj-GGCvJPcXA_jpVeakvD7daMK8Pz5v_6t_vIvJWGawx0boZccD5QDmeyCbLDRQM_EVUF1obV_Ao"
  },

  search: null,
  results: null,
  returnFlight: null,
  flightKeys: null,
  reprice: null,
  currentResults: "onwardJourneys",
  passengersData: null,
  totalMeal: 0,
  totalBaggage: 0,
  fees: 0,
  kyc: null,
  booking: null
};

function rootReducer(state = initialState, action) {
    console.log(JSON.stringify(action));
  switch (action.type) {
    case "SAVE_TOKEN":
      return {
        ...state,
        token: action.token
      };

    case "SAVE_KYC":
      return {
        ...state,
        kyc: action.kyc
      };

    case "SAVE_SEARCH":
      return {
        ...state,
        search: action.search
      };

    case "SAVE_RESULTS":
      return {
        ...state,
        results: action.results
      };

    case "SAVE_ONWARD":
      return {
        ...state,
        onwardFlight: action.onwardFlight
      };

    case "SAVE_RETURN":
      return {
        ...state,
        returnFlight: action.returnFlight
      };

    case "SET_CURRENT_RESULTS":
      console.log(action);
      return {
        ...state,
        currentResults: action.currentResults
      };

    case "SAVE_REPRICE":
      return {
        ...state,
        reprice: action.reprice
      };

    case "SET_FLIGHT_KEYS":
      return {
        ...state,
        flightKeys: action.keys
      };

    case "SET_PASSENGER_DATA":
      return {
        ...state,
        passengersData: action.passengersData
      };

    case "SET_TOTAL_MEAL":
      return {
        ...state,
        totalMeal: action.totalMeal
      };

    case "SET_TOTAL_BAGGAGE":
      return {
        ...state,
        totalBaggage: action.totalBaggage
      };

    case "CALCULATE_FEE":
      return {
        ...state,
        fees: action.fees
      };

    case "BOOK_FLIGHT":
      console.log(action.booking.refId, ' ->>>> booking');
      return {
        ...state,
        booking: action.booking
      };

    default:
      return state;
  }
}

export default rootReducer;
