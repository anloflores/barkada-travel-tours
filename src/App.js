import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import "./assets/css/style.css"

import Home from './pages/Home'
import Results from './pages/Results'
import Search from "./pages/Search";
import Passengers from "./pages/Passengers";
import Addons from "./pages/Addons";
import Confirm from "./pages/Confirm";
import Finish from "./pages/Finish";
import Email from "./pages/Email";

import axios from 'axios'

export default function App() {

  return (
    <Router>
      <div>
        {/* <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
      </ul> */}

        <Switch>
          <Route exact path="/">
            <Home />
          </Route>

          <Route path="/search/tour">
            <Email />
          </Route>

          <Route path="/search/hotel">
            <Email />
          </Route>

          <Route path="/search/flight">
            <Search />
          </Route>

          <Route path="/flight/results">
            <Results />
          </Route>

          <Route path="/flight/addon">
            <Addons />
          </Route>

          <Route path="/flight/passengers">
            <Passengers />
          </Route>

          <Route path="/flight/confirm">
            <Confirm />
          </Route>

          <Route path="/flight/finish">
            <Finish />
          </Route>
        </Switch>
      </div>
      <div className="text-center small">
        v1.0.3
      </div>
    </Router>
  );
}
