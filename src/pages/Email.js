import React, {useState, useEffect} from "react";
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import axios from "axios";

import { connect } from "react-redux";
import { saveToken, saveResult, saveKYC } from "../redux/actions/actions";

import { useHistory, useParams } from "react-router-dom";

import moment from 'moment'


function Email(props) {
  let history = useHistory();
  let { id } = useParams();

    const [test, setTest] = useState(null);

    const [fullname, setFullname] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const [topic, setTopic] = useState('');
    const [content, setContent] = useState('');

    // useEffect(() => {
    //   async function anyNameFunction() {
    //     await props.saveToken();
    //     setTest(true);
    //   }
    //   anyNameFunction();
    // }, []);

    useEffect(() => {
      // history.push("/PTKM/flight_search");
    }, [])
    
    return (
      <div>
        <div className="header">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <h1 className="title text-white">Inquire</h1>
              </div>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="search-holder">
            <div className="row">
              <div className="col-2">
                <i className="input-icon fa fa-2x fa-fw fa-user"> </i>
              </div>
              <div className="col-10">
                <label className="search-label" htmlFor="">
                  Full Name
                </label>
                <input
                  type="text"
                  className="form-control"
                  onChange={v => setFullname(v.target.value)}
                  value={fullname}
                />
              </div>
            </div>

            <div className="row">
              <div className="col-2">
                <i className="input-icon fa fa-2x fa-fw fa-envelope"> </i>
              </div>
              <div className="col-10">
                <label className="search-label" htmlFor="">
                  Email Address
                </label>
                <input
                  type="text"
                  className="form-control"
                  onChange={v => setEmail(v.target.value)}
                  value={email}
                />
              </div>
            </div>

            <div className="row">
              <div className="col-2">
                <i className="input-icon fa fa-2x fa-fw fa-mobile"> </i>
              </div>
              <div className="col-10">
                <label className="search-label" htmlFor="">
                  Mobile Number
                </label>
                <input
                  type="text"
                  className="form-control"
                  onChange={v => setMobile(v.target.value)}
                  value={mobile}
                />
              </div>
            </div>

            <div className="row">
              <div className="col-2">
                <i className="input-icon fa fa-2x fa-fw fa-info"> </i>
              </div>
              <div className="col-10">
                <label className="search-label" htmlFor="">
                  Topic
                </label>
                <input
                  type="text"
                  className="form-control"
                  onChange={v => setTopic(v.target.value)}
                  value={topic}
                />
              </div>
            </div>

            <div className="row">
              <div className="col-12">
                <label className="search-label" htmlFor="">
                  Message
                </label>
                <textarea
                  rows="3"
                  className="form-control"
                  onChange={v => setContent(v.target.value)}
                  value={content}
                  placeholder="Message"
                ></textarea>
              </div>
            </div>

            <div className="row">
              <div className="col-12 text-center">
                <br />
                <br />
                <button
                  onClick={() => {
                    let params = {
                      'firstname': fullname,
                      'email': email,
                      'mobile': mobile,
                      'concern': topic,
                      'content': content,
                      'subject': topic
                    };

                    var form_data = new FormData();

                    for (var key in params) {
                      form_data.append(key, params[key]);
                    }


                    axios({
                      url: "http://barkada.ph/pitakamo2/PTKM/email/1",
                      method: "post",
                      data: form_data
                    }).then(response => {
                      // let r = JSON.parse(response);

                      // console.log(response, ' thsu isasfa sf afaf sf asf asf ');

                      if (response.data.t === true) {
                        alert("Inquiry has been successfully sent");
                        setFullname("");
                        setEmail("");
                        setTopic("");
                        setMobile("");
                        setContent("");
                      } else {
                        alert(response.data.c);
                      }
                    });
                  }}
                  className="btn-float-right"
                >
                  <i className="fa fa-paper-plane"> </i> Send Inquiry
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
}

// export default Home;

const mapStateToProps = state => {
  return {
    token: state.token
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveToken: () => dispatch(saveToken()),
    saveResult: searchData => dispatch(saveResult(searchData)),
    saveKYC: id => dispatch(saveKYC(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Email);