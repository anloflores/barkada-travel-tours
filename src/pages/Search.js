import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";

import { connect } from "react-redux";
import { saveToken, saveResult, saveSearch, co_url } from "../redux/actions/actions";

import { useHistory } from "react-router-dom";

import moment from "moment";

function Search(props) {
  let history = useHistory();

  const [flightType, setFlightType] = useState(1);
  const [data, setData] = useState(null);
  const [departureDate, setDepartureDate] = useState(moment().add('1', 'days').format('YYYY-MM-DD'));
  const [returnDate, setReturnDate] = useState(moment().add('3', 'days').format('YYYY-MM-DD'));

  const [adtCount, setAdtCount] = useState(1);
  const [chdCount, setChdCount] = useState(0);
  const [infCount, setInfCount] = useState(0);

  const [seatClass, setSeatClass] = useState("ECONOMY");

  const [prefAirline, setPrefAirline] = useState('');

  const [fromCode, setFromCode] = useState("MNL");
  const [toCode, setToCode] = useState("DVO");

  const [fromDescription, setFromDescription] = useState("Manila, Philippines");
  const [toDescription, setToDescription] = useState("Davao, Philippines");

  const [fromSuggestion, setFromSuggestion] = useState([]);
  const [toSuggestion, setToSuggestion] = useState([]);

  const [fromSuggestionShow, setFromSuggestionShow] = useState(false);
  const [toSuggestionShow, setToSuggestionShow] = useState(false);

  const [isValidFrom, setIsValidFrom] = useState(true);
  const [isValidTo, setIsValidTo] = useState(true);

  useEffect(() => {
    console.log(props.search)

    if(props.search != null) {
        setFlightType(props.search.flightType);
        setDepartureDate(props.search.departureDate);
        setReturnDate(props.search.returnDate);
        setAdtCount(props.search.adtCount);
        setChdCount(props.search.chdCount);
        setInfCount(props.search.infCount);
        setSeatClass(props.search.seatClass);
        setPrefAirline(props.search.prefAirlines);
        setFromCode(props.search.fromCode);
        setToCode(props.search.toCode);
        setFromDescription(props.search.fromDescription);
        setToDescription(props.search.toDescription);
    }
  }, [])

  const _searchResult = async () => {
    if (isValidFrom && isValidTo) {
      var searchData = {
        from: fromCode,
        to: toCode,
        adt: adtCount,
        chd: chdCount,
        inf: infCount,
        dd: moment(departureDate).format("M-D-YYYY"),
        prefAirlines: prefAirline,
        st: seatClass
      };

      if (flightType == 1) {
        searchData["rd"] = moment(returnDate).format("M-D-YYYY");
        searchData["rt"] = true;
      }

      props.saveSearch({
        fromCode: fromCode,
        toCode: toCode,
        fromDescription: fromDescription,
        toDescription: toDescription,
        adtCount: adtCount,
        chdCount: chdCount,
        infCount: infCount,
        departureDate: departureDate,
        seatClass: seatClass,
        flightType: flightType,
        returnDate: returnDate,
        prefAirlines: prefAirline
      });
      
      await props.saveResult(searchData);
      history.push("/flight/results");
      console.log("test");
    } else {
      alert("Invalid Places");
    }
  };

  useEffect(() => {
    if (props.token === null) {
      // history.push("/");
    }
  }, []);

  const _searchPlaces = (str, or) => {
    if (str.length < 3) {
      if (or == "from") {
        setFromSuggestion([]);
        setFromSuggestionShow(false);
      } else {
        setToSuggestion([]);
        setToSuggestionShow(false);
      }
      return false;
    }

    axios
      .get(props.co_url() + "/api/flights/autofill?term=" + str, {
        headers: {
          Authorization: "Bearer " + props.token.access_token
        }
      })
      .then(res => {
        let d = JSON.parse(res.data.slice(0, -1));
        if (d.apts.length == 0) {
          d.apts = [
            {
              code: "No Available Airport",
              country: "",
              name: "Please try different places.",
              selectable: false
            }
          ];
        }
        if (or == "from") {
          setFromSuggestion(d.apts);
          setFromSuggestionShow(true);
        } else {
          setToSuggestion(d.apts);
          setToSuggestionShow(true);
        }
        console.log();
      });
  };

  return (
    <div>
      <div className="header">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <h1 className="title text-white">Book your flight</h1>
              <ul className="service-type-selection">
                <li
                  onClick={() => {
                    setFlightType(1);
                  }}
                  className={flightType == 1 ? "active" : ""}
                >
                  Roundtrip
                </li>
                <li
                  onClick={() => {
                    setFlightType(0);
                  }}
                  className={flightType == 0 ? "active" : ""}
                >
                  One-Way
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="search-holder">
          <div className="row">
            <div className="col-2">
              <i className="input-icon fa fa-2x fa-fw fa-map-marker"> </i>
            </div>
            <div className="col-5 suggestion-container">
              <label className="search-label" htmlFor="">
                From
              </label>
              <input
                type="text"
                className="form-control"
                onChange={v => {
                  setFromCode(v.target.value);
                  _searchPlaces(v.target.value, "from");
                }}
                value={fromCode}
              />
              <small className="search-description">{fromDescription}</small>
              <div
                className={
                  "suggestion-box" + " " + (fromSuggestionShow ? "active" : "")
                }
              >
                <ul>
                  {fromSuggestion.map((v, i) => {
                    return (
                      <li
                        onClick={() => {
                          if (v.selectable != null) {
                            setIsValidFrom(false);
                            return false;
                          }
                          setFromCode(v.code);
                          setFromDescription(v.name + ", " + v.country);
                          setFromSuggestionShow(false);
                          setIsValidFrom(true);
                        }}
                        key={i}
                      >
                        <p className="code">{v.code}</p>
                        <p className="description">
                          {v.name}
                          <br />
                          {v.country}
                        </p>
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
            <div className="col-5 suggestion-container">
              <label className="search-label" htmlFor="">
                To
              </label>
              <input
                type="text"
                className="form-control"
                onChange={v => {
                  setToCode(v.target.value);
                  _searchPlaces(v.target.value, "to");
                }}
                value={toCode}
              />
              <small className="search-description">{toDescription}</small>
              <div
                className={
                  "suggestion-box" + " " + (toSuggestionShow ? "active" : "")
                }
              >
                <ul>
                  {toSuggestion.map((v, i) => {
                    return (
                      <li
                        onClick={() => {
                          if (v.selectable != null) {
                            setIsValidTo(false);
                            return false;
                          }
                          setToCode(v.code);
                          setToDescription(v.name + ", " + v.country);
                          setToSuggestionShow(false);
                          setIsValidTo(true);
                        }}
                        key={i}
                      >
                        <p className="code">{v.code}</p>
                        <p className="description">
                          {v.name}
                          <br />
                          {v.country}
                        </p>
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
          </div>
          <br />
          <br />
          <div className="row">
            <div className="col-2">
              <i className="input-icon fa fa-2x fa-fw fa-calendar"> </i>
            </div>
            <div className="col-5">
              <label className="search-label" htmlFor="">
                Departure
              </label>

              <input
                type="date"
                value={departureDate}
                onChange={v => setDepartureDate(v.target.value)}
                min={moment().format("YYYY-MM-DD")}
                className="form-control"
                required="required"
              />
            </div>
            <div
              className="col-5"
              style={flightType == 0 ? { display: "none" } : {}}
            >
              <label className="search-label" htmlFor="">
                Return
              </label>
              <input
                type="date"
                value={returnDate}
                onChange={v => setReturnDate(v.target.value)}
                min={moment(departureDate).format("YYYY-MM-DD")}
                className="form-control"
                required="required"
              />
            </div>
          </div>
          <br />
          <div className="row">
            <div className="col-2">
              <i className="input-icon fa fa-2x fa-fw fa-users"> </i>
            </div>
            <div className="col-10">
              <div className="row">
                <div className="col-4">
                  <label className="search-label" htmlFor="">
                    Adult
                  </label>
                  <input
                    type="number"
                    min="0"
                    max="10"
                    value={adtCount}
                    onChange={v => setAdtCount(v.target.value)}
                    className="form-control"
                  />
                  <small className="search-label passenger-label">
                    12 years+
                  </small>
                </div>
                <div className="col-4">
                  <label className="search-label" htmlFor="">
                    Children
                  </label>
                  <input
                    type="number"
                    min="0"
                    max="10"
                    value={chdCount}
                    onChange={v => setChdCount(v.target.value)}
                    className="form-control"
                  />
                  <small className="search-label passenger-label">
                    2 to 11 years
                  </small>
                </div>
                <div className="col-4">
                  <label className="search-label" htmlFor="">
                    Infant
                  </label>
                  <input
                    type="number"
                    min="0"
                    max="10"
                    value={infCount}
                    onChange={v => setInfCount(v.target.value)}
                    className="form-control"
                  />
                  <small className="search-label passenger-label">
                    Under 2 years
                  </small>
                </div>
              </div>
            </div>
          </div>
          <br />
          <div className="row">
            <div className="col-2">
              <i className="input-icon fa fa-2x fa-fw fa-plane"> </i>
            </div>
            <div className="col-10">
              <label className="search-label" htmlFor="">
                Type of Class
              </label>
              <select
                name=""
                id=""
                onChange={v => setSeatClass(v.target.value)}
                className="form-control"
                defaultValue={seatClass}
              >
                <option value="ECONOMY">Economy Class</option>
                <option value="BUSINESS">Business Class</option>
                <option value="FIRST">First Class</option>
              </select>
            </div>
          </div>

          <div className="row">
            <div className="col-12 text-center">
              <br/>
              Show More Options
              <br/><br/>
            </div>
          </div>

          <div className="row">
            <div className="col-2">
              <i className="input-icon fa fa-2x fa-fw fa-plane"> </i>
            </div>
            <div className="col-10">
              <label className="search-label" htmlFor="">
                Preffered Airline (Atleast 3 Letters)
              </label>
               <input
                    value={prefAirline}
                    onChange={v => setPrefAirline(v.target.value)}
                    className="form-control"
                  />
            </div>
          </div>
        </div>
      </div>

      <div className="clearfix"></div>
      <div className="text-center">
        <button onClick={() => _searchResult()} className="btn-float-right">
          <i className="fa fa-search"> </i> Search Flight
        </button>
      </div>
      <div className="clearfix"></div>
      <br />
      <br />
    </div>
  );
}

// export default Search;

const mapStateToProps = state => {
  return {
    token: state.token,
    search: state.search,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveToken: () => dispatch(saveToken()),
    saveResult: searchData => dispatch(saveResult(searchData)),
    saveSearch: searchData => dispatch(saveSearch(searchData)),
    co_url: () => {return co_url}
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
