import React, { useState, useEffect } from "react";
import axios from "axios";

import { connect } from "react-redux";
import { setPassengerData, setTotalBaggage, setTotalMeal } from "../redux/actions/actions";
import { useHistory } from "react-router-dom"

import NumberFormat from "react-number-format";

function Addons(props) {
  let history = useHistory();

  const [baggageModal, setBaggageModal] = useState(false);
  const [mealModal, setMealModal] = useState(false);

  const [baggageArr, setBaggageArr] = useState([]);
  const [mealArr, setMealArr] = useState([]);

  const [currentIndex, setCurrentIndex] = useState(null);


  useEffect(() => {
    if(props.search.fromCode == null) {
      history.push("/search/flight");
    }


    // let ba = baggageArr;
    // let ma = mealArr;
    // props.passengersData.map((v, i) => {
    //   console.log(i)
    //   ba[i] = [];
    //   ma[i] = [];
    // });

    // setBaggageArr(ba);
    // setMealArr(ma);

    // console.log(baggageArr);
    // console.log(mealArr);
  }, [])

  useEffect(() => {
    console.log(baggageArr);
  })

  const baggageSelection = () => {
    var c = [];
    if(props.reprice.ssr.ssrHeap.baggage == null) return (<li className="list-group-item text-center">No Available Baggage Selection</li>);

     props.reprice.ssr.ssrHeap.baggage.map(v => {
      let ways = [];
      let waysString = "";
       v.appFltKeys.map(k => {
         ways.push(
           <span className="badge badge-primary" style={{ marginRight: "5px" }}>
             {props.reprice.flights[k].depDetail.code}{" "}
             <i className="fa fa-fw fa-arrow-right"> </i>{" "}
             {props.reprice.flights[k].arrDetail.code} (
             <strong>{props.reprice.flights[k].carrier.code + '-' + props.reprice.flights[k].flightNo}</strong>)
           </span>
         );

         waysString += props.reprice.flights[k].flightNo + " ";
       });

       c.push(
         <li className="list-group-item list-group-item-secondary">{ways}</li>
       );

       v.data.map(d => {
         console.log(d, ' dddddd ');
         c.push(
           <li
             className="list-group-item small"
             onClick={() => {
               let ba = baggageArr;
               d["key"] = v.key;
               if (ba[currentIndex] == null) {
                 ba[currentIndex] = [];
                 ba[currentIndex].push(d);
               } else {
                 let check = false;

                 for (let i = 0; i < ba[currentIndex].length; i++) {
                   if (ba[currentIndex][i].key == d.key) {
                     d["dest"] = waysString;
                     ba[currentIndex][i] = d;
                     check = true;
                     break;
                   } else {
                     check = false;
                   }
                 }

                 if (!check) ba[currentIndex].push(d);
               }

               setBaggageArr(ba);
               setBaggageModal(false);
             }}
           >
             <div className="float-left">{d.desc}</div>
             <div className="float-right">
               <NumberFormat
                 value={d.amount}
                 displayType={"text"}
                 thousandSeparator={true}
                 prefix={"₱"}
               />
             </div>
             <div className="claerfix"></div>
           </li>
         );
       })
     });

     return c;
  }

  const mealSelection = () => {
    var c = [];
    if (props.reprice.ssr.ssrHeap.meal == null) return (<li className="list-group-item text-center">No Available Meal Selection</li>);
     props.reprice.ssr.ssrHeap.meal.map(v => {
      let ways = [];
      let waysString;
       v.appFltKeys.map(k => {
         ways.push(
           <span className="badge badge-primary">
             {props.reprice.flights[k].depDetail.code}{" "}
             <i className="fa fa-fw fa-arrow-right"> </i>{" "}
             {props.reprice.flights[k].arrDetail.code}
           </span>
         );

         waysString += props.reprice.flights[k].flightNo + " ";
       });

       c.push(
         <li className="list-group-item list-group-item-secondary">{ways}</li>
       );

       v.data.map(d => {
         c.push(
           <li
             className="list-group-item small"
             onClick={() => {
               let ma = mealArr;
               d["key"] = v.key;
               if (ma[currentIndex] == null) {
                 ma[currentIndex] = [];
                 ma[currentIndex].push(d);
               } else {
                 let check = false;

                 for (let i = 0; i < ma[currentIndex].length; i++) {
                   if (ma[currentIndex][i].key == d.key) {
                     d["dest"] = waysString;
                     ma[currentIndex][i] = d;
                     check = true;
                     break;
                   } else {
                     check = false;
                   }
                 }

                 if (!check) ma[currentIndex].push(d);
               }

               setMealArr(ma);
               setMealModal(false);
             }}
           >
             <div className="float-left">{d.desc}</div>
             <div className="float-right">
               <NumberFormat
                 value={d.amount}
                 displayType={"text"}
                 thousandSeparator={true}
                 prefix={"₱"}
               />
             </div>
             <div className="claerfix"></div>
           </li>
         );
       })
     });

     return c;
  }

  const layout = () => {
    try {
      return (
        <div>
          <div className="header">
            <div className="container-fluid search-result-holder">
              <div className="row">
                <div className="col-5">
                  <h1 className="title-2x m-0 text-white search-code">
                    {props.search.fromCode}
                  </h1>
                  <small className="text-header-darker m-0 search-description">
                    {props.search.fromDescription}
                  </small>
                </div>
                <div className="col-2">
                  <br />
                  <i className="fa fa-2x fa-fw fa-plane text-header-darker"></i>
                </div>
                <div className="col-5">
                  <h1 className="title-2x m-0 text-white search-code">
                    {props.search.toCode}
                  </h1>
                  <small className="text-header-darker m-0 search-description">
                    {props.search.toDescription}
                  </small>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-7">
                  <h1 className="title text-white">
                    <div>
                      {props.search.adtCount}Adult, {props.search.chdCount}
                      Child, {props.search.infCount}Infant
                    </div>
                    <small className="text-header-darker">Passengers</small>
                  </h1>
                </div>
                <div className="col-5">
                  <h1 className="title text-white">
                    <div>{props.search.seatClass}</div>
                    <small className="text-header-darker">Type of Class</small>
                  </h1>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-12 text-center">
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={() => history.push("/search/flight")}
                  >
                    <i className="fa fa-search fa-fw"> </i> Change Search
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-12">
                <h1 className="title">Add Ons</h1>
                {props.passengersData.map((v, i) => {
                  return (
                    <div className="card traveller-addon">
                      <div className="card-header">
                        <div className="row">
                          <div className="col-1">
                            <i className="fa fa-fw fa-user"></i>
                          </div>
                          <div
                            className="col-10"
                            style={{ textTransform: "capitalize" }}
                          >
                            {v.title + " " + v.firstName + " " + v.lastName}
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="row">
                          <div className="col-12">
                            {baggageArr[i] != null || mealArr[i] != null ? (
                              <table class="table table-bordered table-padding-0 small">
                                {baggageArr[i].length >= 0
                                  ? baggageArr[i].map((b) => {
                                      return (
                                        <tr>
                                          <td>{b.key.split("_").join(" ")}</td>
                                          <td>Baggage</td>
                                          <td>{b.desc}</td>
                                          <td>
                                            <NumberFormat
                                              value={b.amount}
                                              displayType={"text"}
                                              thousandSeparator={true}
                                              prefix={"₱"}
                                            />
                                          </td>
                                        </tr>
                                      );
                                    })
                                  : "null"}
                                {mealArr[i] != null
                                  ? mealArr[i].map((m) => {
                                      return (
                                        <tr>
                                          <td>{m.key.split("_").join(" ")}</td>
                                          <td>Meal</td>
                                          <td>{m.desc}</td>
                                          <td>
                                            <NumberFormat
                                              value={m.amount}
                                              displayType={"text"}
                                              thousandSeparator={true}
                                              prefix={"₱"}
                                            />
                                          </td>
                                        </tr>
                                      );
                                    })
                                  : ""}
                              </table>
                            ) : (
                              ""
                            )}
                          </div>
                          <div className="col-6 text-center small">
                            <button
                              className="btn btn-info btn-sm small"
                              style={{ fontSize: "12px" }}
                              onClick={() => {
                                setCurrentIndex(i);
                                setBaggageModal(true);
                              }}
                            >
                              {baggageArr[i] == null ? "Add" : "Update"} Baggage
                            </button>
                          </div>
                          <div className="col-6 text-center">
                            <button
                              className="btn btn-info btn-sm small"
                              style={{ fontSize: "12px" }}
                              onClick={() => {
                                setCurrentIndex(i);
                                setMealModal(true);
                              }}
                            >
                              {mealArr[i] == null ? "Add" : "Update"} Meal
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>

              <div className="col-12 text-center">
                <button
                  className="btn btn-primary btn-sm"
                  onClick={() => {
                    let pd = props.passengersData;
                    let totalMeal = 0;
                    let totalBaggage = 0;
                    pd.map((v, i) => {
                      pd[i]["ssr"] = [];
                      if (baggageArr[i]) {
                        baggageArr[i].map((b) => {
                          pd[i]["ssr"].push({
                            ssrType: "baggage",
                            code: b.code,
                            key: b.key,
                          });

                          totalBaggage += parseInt(b.amount);
                        });
                      }

                      if (mealArr[i]) {
                        mealArr[i].map((m) => {
                          pd[i]["ssr"].push({
                            ssrType: "meal",
                            code: m.code,
                            key: m.key,
                          });

                          totalMeal += parseInt(m.amount);
                        });
                      }
                    });

                    props.setTotalBaggage(totalBaggage);
                    props.setTotalMeal(totalMeal);
                    console.log(totalMeal, totalBaggage, " -- addons");
                    props.setPassengerData(pd);
                    history.push("/flight/confirm");
                  }}
                >
                  Continue <i className="fa fa-fw fa-chevron-right"> </i>
                </button>
              </div>
            </div>
          </div>

          <div className={"bt-modal " + (baggageModal ? "visible" : "")}>
            <div className="bt-modal-body">
              <div className="conatiner">
                <div className="col-12">
                  <h1 className="title float-left">Add Baggage</h1>
                  <i
                    className="fa fa-fw fa-lg fa-close float-right"
                    style={{ marginTop: "8px" }}
                    onClick={() => setBaggageModal(false)}
                  ></i>
                  <div className="clearfix"></div>
                </div>
                <br />
                <ul className="list-group">{baggageSelection()}</ul>
              </div>
            </div>
          </div>

          <div className={"bt-modal " + (baggageModal ? "visible" : "")}>
            <div className="bt-modal-body">
              <div className="conatiner">
                <div className="col-12">
                  <h1 className="title float-left">Add Baggage</h1>
                  <i
                    className="fa fa-fw fa-lg fa-close float-right"
                    style={{ marginTop: "8px" }}
                    onClick={() => setBaggageModal(false)}
                  ></i>
                  <div className="clearfix"></div>
                </div>
                <br />
                <ul className="list-group">{baggageSelection()}</ul>
              </div>
            </div>
          </div>

          <div className={"bt-modal " + (mealModal ? "visible" : "")}>
            <div className="bt-modal-body">
              <div className="conatiner">
                <div className="col-12">
                  <h1 className="title float-left">Add Meal</h1>
                  <i
                    className="fa fa-fw fa-lg fa-close float-right"
                    style={{ marginTop: "8px" }}
                    onClick={() => setMealModal(false)}
                  ></i>
                  <div className="clearfix"></div>
                </div>
                <br />
                <ul className="list-group">{mealSelection()}</ul>
              </div>
            </div>
          </div>
        </div>
      );
    } catch (e) {
      history.push("/search/flight");
    }
  }

  return (
    <div>
      {layout()}
    </div>
  );
}

// export default Home;

const mapStateToProps = state => {
  return {
    token: state.token,
    results: state.results,
    search: state.search,
    onwardFlight: state.onwardFlight,
    returnFlight: state.returnFlight,
    currentResults: state.currentResults,
    reprice: state.reprice,
    passengersData: state.passengersData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setPassengerData: passengerData => dispatch(setPassengerData(passengerData)),
    setTotalMeal: totalMeal => dispatch(setTotalMeal(totalMeal)),
    setTotalBaggage: totalBaggage => dispatch(setTotalBaggage(totalBaggage))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Addons);
