import React, {useState, useEffect} from "react";
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import axios from "axios";

import { connect } from "react-redux";

import { useHistory, useParams } from "react-router-dom";

import moment from 'moment'


function Finish(props) {
  let history = useHistory();

    useEffect(() => {
      console.log(props, '->>>>>>>>>> finish props');
    }, [])
    
    return (
      <div>
        {
          console.log(props, '->>>>>>>>>> finish props')
        }
        <div className="header">
          <div className="container-fluid text-center">
            <h1 className="title text-white">
              <div>Thank you for Booking your flight with us!</div>
              <small className="small" style={{ paddingTop: "10px" }}>
                Kindly check your PitakaMo Registered Email Address
                <br /> for your Flight Ticket.
              </small>
            </h1>
          </div>
        </div>

        <div className="container-fluid">
          <div className="row">
            <div className="col-12 text-center">
              <br />
              <h1 className="title">
                <div>{props.booking.refId}</div>
                <small className="text-header-darker">Reference #</small>
              </h1>
              <button
                className="btn btn-primary btn-sm small"
                onClick={() => history.push("/search/flight")}
              >
                Complete Booking
              </button>
              <br />
              <br />
            </div>
            <div className="col-12">
              <p className="small">
                For any concerns or questions, please contact:
                <br />
                info.barkada@gmail.com
                <br /> ales@barkada.travel
                <br />
                barkadatravelandtours@gmail.com
                <br />
                <br />
                +63 2 711 75 24
                <br />
                +63 997 506 8145
                <br />
                +63 929 270 4961
                <br />
                <br />
                Unit B Gonzales Bldg. 1359 Sto Sepulcro
                <br />
                St. Paco Manila
                <br />
                <br />
                Office Hours:
                <br />
                Mon – Fri : 9AM – 6PM
                <br />
                Sat : 9AM –2PM
              </p>
            </div>
          </div>
        </div>
      </div>
    );
}

// export default Home;

const mapStateToProps = state => {
  return {
    token: state.token,
    booking: state.booking
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Finish);