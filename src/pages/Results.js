import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";

import { connect } from "react-redux";
import { saveToken, saveResult, saveOnwardFlight, saveReturnFlight, repriceFlight, setFlightKeys } from "../redux/actions/actions";
import { useHistory } from "react-router-dom"
import moment from "moment";

import Flights from '../components/Flights';
import NumberFormat from "react-number-format";

function Results(props) {
  let history = useHistory();
  const [totalCount, setTotalCount] = useState(0);
  let [flightResult, setFlightResult] = useState([]);
  let [flightResultTemp, setFlightResultTemp] = useState([]);
  const [currentType, setCurrentType] = useState('onward');
  const [selectedOnward, setSelectedOnward] = useState(null);
  const [selectedReturn, setSelectedReturn] = useState(null);
  const [isCombinedFlight, setIsCombinedFlight] = useState(false);

  useEffect(() => {
    if(props.results == null) {
      history.push("/search/flight");
    }

    try {
      if (props.results.isCombined || false) {
        setCurrentType("combined");
        // q(props.results.combinedJourneys);
        setFlightResultTemp(props.results.combinedJourneys);
        setTotalCount(props.results.combinedJourneys.length);
      } else {
        setTotalCount(props.results.onwardJourneys.length);
        setFlightResult(props.results.onwardJourneys);
        setFlightResultTemp(props.results.onwardJourneys);
      }
    } catch(e) {

    }
  }, [])

  useEffect(() => {
    console.log(flightResult);
  }, [flightResult])

  const repriceFlights = async () => {
    let keys = [selectedOnward.key];
    if (props.search.flightType == 1) {
      keys.push(selectedReturn.key);
    }

    props.setFlightKeys(keys);
    await props.repriceFlight();
    history.push('/flight/passengers')
  };  

  const flightItems = (sub) => {
    
    if(sub.flight == null || sub.flight.length == 0) return '';
    return (
      <div className="flight-item">
        <div className="container">
          {sub.flight.flights.map((flight, i) => {
            return (
              <div key={i}>
                <div className="row">
                  <div className="col-12 flight-icons">
                    <img
                      className="float-left"
                      src={
                        "http://btt.complexus.tech/images/airlines/" +
                        flight.carrier.code +
                        ".gif"
                      }
                      alt=""
                    />
                    <h1 className="title airline float-left">
                      <div>{flight.carrier.name}</div>
                      <small className="small">{sub.flight.flightKey.split('#').join(' ')}</small>
                    </h1>

                    {/* <div className="float-right">
                        <i className="fa fa-fw fa-2x question fa-question-circle"> </i>
                      </div> */}
                    <div className="clearfix"></div>
                  </div>
                </div>
                <br />
                <hr />
                <br />
                <div className="row">
                  <div className="col-4 text-right">
                    <h1 className="title text-right">
                      <div>{moment(flight.depDetail.time).format("HH:mm")}</div>
                      <small>{flight.depDetail.code}</small>
                      <small style={{fontSize: '8px', display: 'block'}}>{ moment(flight.depDetail.time).format('MM/DD/YYYY') }</small>
                    </h1>
                  </div>
                  <div className="col-4 text-center">
                    <ul>
                      <li>
                        {Math.floor(flight.flyTime / 60)}h{" "}
                        {Math.floor(flight.flyTime % 60)}
                      </li>
                    </ul>

                    {flight.stops != 0 ? (
                      <small>{flight.stops} STOP</small>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="col-4">
                    <h1 className="title">
                      <div>
                        {moment(flight.arrDetail.time).format("HH:mm")}{" "}
                        {flight.nextDatArr ? <sup>+1</sup> : ""}
                      </div>
                      <small>{flight.arrDetail.code}</small>
                      <small style={{fontSize: '8px', display: 'block'}}>{ moment(flight.arrDetail.time).format('MM/DD/YYYY') }</small>
                    </h1>
                  </div>
                </div>
                <br />
                <hr />
              </div>
            );
          })}

          <div className="row flight-icons">
            <div className="col-12">
              {sub.check == null ? (
                <button
                  className="btn btn-primary btn-sm float-right"
                  onClick={() => {
                    if (sub.type == "onward") {
                      if (props.search.flightType == 1) {
                        setTotalCount(props.results.returnJourneys.length);
                        setFlightResult(props.results.returnJourneys);
                        setCurrentType("return");
                      } else {
                        setFlightResult([]);
                        setSelectedReturn([]);
                      }

                      setSelectedOnward(sub.flight);
                      saveOnwardFlight(sub.flight);
                    } else if (sub.type == "return") {
                      if (props.search.flightType == 1) {
                        setSelectedReturn(sub.flight);
                        saveReturnFlight(sub.flight);
                        setFlightResult([]);
                      }
                    } else if (sub.type == "combined") {
                      setFlightResult([]);
                      setSelectedReturn([]);
                      setSelectedOnward(sub.flight);
                      saveOnwardFlight(sub.flight);
                    }
                  }}
                >
                  Select
                </button>
              ) : (
                <button
                  className="btn btn-primary btn-sm float-right"
                  onClick={() => {
                    if (selectedReturn != null || selectedReturn.length >= 1) {
                      if (
                        !window.confirm(
                          "Are you sure you want to unselect the Departure Flight? By Continuing the Return Flight will also be unselected."
                        )
                      )
                        return false;
                    }

                    if (sub.type == "onward") {
                      setTotalCount(props.results.onwardJourneys.length);
                      setFlightResult(props.results.onwardJourneys);
                      setCurrentType("onward");
                      setSelectedOnward(null);
                      saveOnwardFlight(null);
                      setSelectedReturn(null);
                      saveReturnFlight(null);
                    } else if (sub.type == "return") {
                      if (props.search.flightType == 1) {
                        setTotalCount(props.results.returnJourneys.length);
                        setFlightResult(props.results.returnJourneys);
                        setCurrentType("return");
                        setSelectedReturn(null);
                        saveReturnFlight(null);
                      }
                    } else if (sub.type == "combined") {
                      setCurrentType("combined");
                      setFlightResult(props.results.combinedJourneys);
                      setTotalCount(props.results.combinedJourneys.length);
                      setSelectedOnward(null);
                      saveOnwardFlight(null);
                      setSelectedReturn(null);
                      saveReturnFlight(null);
                    }
                  }}
                >
                  Unselect
                </button>
              )}
              <h1 className="title float-left">
                <NumberFormat
                  value={sub.flight.fares.totalFare.total.amount}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"₱"}
                />
              </h1>
              <div className="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  const layout = () => {
    try {
      return (
        <div>
          <div className="header">
            <div className="container-fluid search-result-holder">
              <div className="row">
                <div className="col-5">
                  <h1 className="title-2x m-0 text-white search-code">
                    {props.search.fromCode}
                  </h1>
                  <small className="text-header-darker m-0 search-description">
                    {props.search.fromDescription}
                  </small>
                </div>
                <div className="col-2">
                  <br />
                  <i className="fa fa-2x fa-fw fa-plane text-header-darker"></i>
                </div>
                <div className="col-5">
                  <h1 className="title-2x m-0 text-white search-code">
                    {props.search.toCode}
                  </h1>
                  <small className="text-header-darker m-0 search-description">
                    {props.search.toDescription}
                  </small>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-7">
                  <h1 className="title text-white">
                    <div>
                      {props.search.adtCount}Adult, {props.search.chdCount}Child,{" "}
                      {props.search.infCount}Infant
                    </div>
                    <small className="text-header-darker">Passengers</small>
                  </h1>
                </div>
                <div className="col-5">
                  <h1 className="title text-white">
                    <div>{props.search.seatClass}</div>
                    <small className="text-header-darker">Type of Class</small>
                  </h1>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-12 text-center">
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={() => history.push("/search/flight")}
                  >
                    <i className="fa fa-search fa-fw"> </i> Change Search
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <br />
                <h1 className="title float-left">
                  <span className="text-header-darker">{totalCount}</span>
                  <small className="small"> results sorted by cheapest</small>
                </h1>
                <i className="fa fa-sliders fa-fw fa-2x float-right text-header-darker"></i>
                <div className="clearfix"></div>
                <br />
              </div>


              <h1 className="title">Filter Flights</h1>
              <div className="col-md-12 result-filter">
                <div
                  onClick={async () => {
                    await setFlightResult(flightResultTemp);
                    flightResult = flightResultTemp;
                    var t3st = flightResult;
                    let filtered = t3st.filter((v, i) => {
                      var time = new Date(
                        v.flights[0].depDetail.time
                      ).getHours();
                      console.log(time);
                      return time >= 1 && time <= 5;
                    });

                    setFlightResult(filtered);
                  }}
                >
                  Early Morning
                </div>
                <div
                  onClick={async () => {
                    await setFlightResult(flightResultTemp);
                    flightResult = flightResultTemp;
                    var t3st = flightResult;
                    let filtered = t3st.filter((v, i) => {
                      var time = new Date(
                        v.flights[0].depDetail.time
                      ).getHours();
                      console.log(time);
                      return time >= 5 && time <= 12;
                    });

                    setFlightResult(filtered);
                  }}
                >
                  Morning
                </div>
                <div
                  onClick={async () => {
                    await setFlightResult(flightResultTemp);
                    flightResult = flightResultTemp;
                    var t3st = flightResult;
                    let filtered = t3st.filter((v, i) => {
                      var time = new Date(
                        v.flights[0].depDetail.time
                      ).getHours();
                      console.log(time);
                      return time >= 12 && time <= 18;
                    });

                    setFlightResult(filtered);
                  }}
                >
                  Afternoon
                </div>
                <div
                  onClick={async () => {
                    await setFlightResult(flightResultTemp);
                    flightResult = flightResultTemp;
                    var t3st = flightResult;
                    let filtered = t3st.filter((v, i) => {
                      var time = new Date(
                        v.flights[0].depDetail.time
                      ).getHours();
                      console.log(time);
                      return time >= 18 && time <= 24;
                    });

                    setFlightResult(filtered);
                  }}
                >
                  Evening
                </div>
                <div
                  onClick={() => {
                    setFlightResult(flightResultTemp);
                  }}
                >
                  Clear
                </div>
              </div>
            </div>

            {selectedOnward != null ? (
              <div className="row">
                <div className="col-12">
                  <h1 className="title">
                    {props.results.isCombined || false
                      ? "Combined Flights"
                      : "Departure Flight"}
                  </h1>
                </div>
              </div>
            ) : (
              ""
            )}

            {selectedOnward != null
              ? flightItems({
                  flight: selectedOnward,
                  type:
                    props.results.isCombined || false ? "combined" : "onward",
                  check: false,
                })
              : ""}

            {selectedReturn != null ? (
              <div className="row">
                <div className="col-12">
                  <h1 className="title">
                    {props.results.isCombined || false
                      ? ""
                      : props.results.returnJourneys != null
                      ? "Return Flight"
                      : ""}
                  </h1>
                </div>
              </div>
            ) : (
              ""
            )}

            {selectedReturn != null
              ? flightItems({
                  flight: selectedReturn,
                  type: "return",
                  check: false,
                })
              : ""}

            {isCombinedFlight ? (
              ""
            ) : selectedOnward == null || selectedReturn == null ? (
              <div>
                <div className="row">
                  <div className="col-12">
                    <h1 className="title">
                      {currentType == "combined"
                        ? "Combined Flights"
                        : currentType == "onward"
                        ? "Departure Flight"
                        : "Return Flight"}
                    </h1>
                  </div>
                </div>
                {flightResult.map((v, i) => {
                  return flightItems({ flight: v, type: currentType, key: i });
                })}
              </div>
            ) : (
              ""
            )}

            <div className="row">
              <div className="col-12 text-center">
                {selectedOnward !== null && selectedReturn !== null ? (
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={() => repriceFlights()}
                  >
                    Confirm Flights
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
          <br />
          <br />
        </div>
      );
    } catch (e) {

    }
  }

  return (
    <div>
      {layout()}
    </div>
  );
}

// export default Home;

const mapStateToProps = state => {
  return {
    token: state.token,
    results: state.results,
    search: state.search,
    onwardFlight: state.onwardFlight,
    returnFlight: state.returnFlight,
    currentResults: state.currentResults,
    reprice: state.reprice
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveResult: searchData => dispatch(saveResult(searchData)),
    saveOnwardFlight: onwardFlight => dispatch(saveOnwardFlight(onwardFlight)),
    saveReturnFlight: returnFlight => dispatch(saveReturnFlight(returnFlight)),
    repriceFlight: rf => dispatch(repriceFlight(rf)),
    setFlightKeys: flightKeys => dispatch(setFlightKeys(flightKeys))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Results);
