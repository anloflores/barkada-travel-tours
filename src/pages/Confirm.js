import React, {useState, useEffect} from "react";
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import axios from "axios";

import { connect } from "react-redux";
import { saveToken, saveResult, saveKYC, calculateFee, makePitakaMoPayment, makeBooking } from "../redux/actions/actions";

import { useHistory, useParams } from "react-router-dom";

import moment from 'moment'


function Home(props) {
  let history = useHistory();
  const [total, setTotal] = useState([]);
  const [pin, setPin] = useState([]);

  useEffect(() => {
    try {
      let totalPrice =
        parseInt(props.reprice.fare.totalFare.total.amount) +
        parseInt(props.totalBaggage) +
        parseInt(props.totalMeal);
      let totalPax =
        parseInt(props.reprice.searchQuery.paxCount.adt) +
        parseInt(props.reprice.searchQuery.paxCount.chd) +
        parseInt(props.reprice.searchQuery.paxCount.inf);
      let totalWay = props.reprice.flights.length;
      if (props.kyc == null) {
        history.push("/search/flight");
      } else {
        async function calculateFees() {
          await props
            .calculateFee({
              P_PITAKAMO_ACCOUNT: props.kyc[0].KYC_MOBILE,
              P_AMOUNT: totalPrice,
              P_BAKARDA_SERVICE_CODE: props.results.isDomestic ? 101 : 102,
              P_PAX_COUNT: totalPax,
              P_WAY_COUNT: totalWay,
              P_CAR_COUNT: 0,
              P_BUS_COUNT: 0,
              P_ROOM_COUNT: 0,
              P_NIGHT_COUNT: 0,
              P_DAY_COUNT: 0
            })
            .then(e => {
              if (!e) alert("Something went wrong.");
              setTotal(e);
              console.log(e);
            });
        }
        calculateFees();
      }
    } catch(e) {
      history.push('/search/flight')
    }
  }, [])
    
  const layout = () => {
    try {
      return (
        <div>
          <div className="header">
            <div className="container-fluid search-result-holder">
              <div className="row">
                <div className="col-12">
                  <h1 className="title text-white">
                    <div>Booking Confirmation</div>
                    <small className="text-header-darker"></small>
                  </h1>
                </div>
                <div className="col-5">
                  <h1 className="title-2x m-0 text-white search-code">
                    {props.search.fromCode}
                  </h1>
                  <small className="text-header-darker m-0 search-description">
                    {props.search.fromDescription}
                  </small>
                </div>
                <div className="col-2">
                  <br />
                  <i className="fa fa-2x fa-fw fa-plane text-header-darker"></i>
                </div>
                <div className="col-5">
                  <h1 className="title-2x m-0 text-white search-code">
                    {props.search.toCode}
                  </h1>
                  <small className="text-header-darker m-0 search-description">
                    {props.search.toDescription}
                  </small>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-7">
                  <h1 className="title text-white">
                    <div>
                      {props.search.adtCount}Adult, {props.search.chdCount}
                      Child, {props.search.infCount}Infant
                    </div>
                    <small className="text-header-darker">Passengers</small>
                  </h1>
                </div>
                <div className="col-5">
                  <h1 className="title text-white">
                    <div>{props.search.seatClass}</div>
                    <small className="text-header-darker">Type of Class</small>
                  </h1>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-12 text-center">
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={() => history.push("/search/flight")}
                  >
                    <i className="fa fa-search fa-fw"> </i> Change Search
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-12">
                <label className="page-label">Flights</label>
                {props.reprice.flights.map((flight) => {
                  return (
                    <div className="flight-item">
                      <div className="row">
                        <div className="col-12 flight-icons">
                          <img
                            className="float-left"
                            src={
                              "http://btt.complexus.tech/images/airlines/" +
                              flight.carrier.code +
                              ".gif"
                            }
                            alt=""
                          />
                          <h1 className="title airline float-left">
                            <div>{flight.carrier.name}</div>
                            <small className="small">
                              {flight.carrier.code + "-" + flight.flightNo}
                            </small>
                          </h1>

                          {/* <div className="float-right">
                        <i className="fa fa-fw fa-2x question fa-question-circle"> </i>
                      </div> */}
                          <div className="clearfix"></div>
                        </div>
                      </div>
                      <br />
                      <hr />
                      <br />
                      <div className="row">
                        <div className="col-4 text-right">
                          <h1 className="title text-right">
                            <div>
                              {moment(flight.depDetail.time).format("HH:mm")}
                            </div>
                            <small>{flight.depDetail.code}</small>
                          </h1>
                        </div>
                        <div className="col-4 text-center">
                          <ul>
                            <li>
                              {Math.floor(flight.flyTime / 60)}h{" "}
                              {Math.floor(flight.flyTime % 60)}
                            </li>
                          </ul>

                          {flight.stops != 0 ? (
                            <small>{flight.stops} STOP</small>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="col-4">
                          <h1 className="title">
                            <div>
                              {moment(flight.arrDetail.time).format("HH:mm")}{" "}
                              {flight.nextDatArr ? <sup>+1</sup> : ""}
                            </div>
                            <small>{flight.arrDetail.code}</small>
                          </h1>
                        </div>
                      </div>
                      <br />
                      <hr />
                    </div>
                  );
                })}
              </div>

              <div className="col-12">
                <label className="page-label">Passengers</label>
                {props.passengersData.map((v) => {
                  return (
                    <div className="card">
                      <div
                        className="card-body"
                        style={{ paddingBottom: "0px" }}
                      >
                        <div className="row">
                          <div className="col-1">
                            <i className="fa fa-user fa-fw"> </i>
                          </div>

                          <div className="col-10">
                            <label className="page-label">
                              {v.title + " " + v.firstName + " " + v.lastName}
                            </label>
                          </div>

                          <div className="col-12">
                            <table className="table small">
                              {v.ssr ? (
                                v.ssr.map((s) => {
                                  let details = null;
                                  if (s.ssrType == "meal") {
                                    details = s.code.split("=");
                                  } else if (s.ssrType == "baggage") {
                                    details = s.code.split(";");
                                  }

                                  return (
                                    <tr>
                                      <td>
                                        {details[0]}{" "}
                                        {s.ssrType == "baggage"
                                          ? "Baggage"
                                          : ""}
                                      </td>
                                      <td>{details[1]}</td>
                                    </tr>
                                  );
                                })
                              ) : (
                                <tr>
                                  <td className="text-center">
                                    No Add On Added
                                  </td>
                                </tr>
                              )}
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>

              <div className="col-12">
                <label className="page-label">Fare Details</label>
                <div className="card">
                  <div className="card-body">
                    <div className="row">
                      <div className="col-12">
                        <table className="table small">
                          {total.map((v, i) => {
                            return (
                              <tr>
                                <td>{v.FIELD_NAME}</td>
                                <td className="text-right">{v.FIELD_VALUE}</td>
                              </tr>
                            );
                          })}
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 form-group">
                <label className="page-label">PitakaMo Account PIN</label>
                <input
                  type="password"
                  pattern="[0-9]"
                  className="form-control"
                  placeholder="PitakaMo Pin"
                  onChange={(v) => {
                    setPin(v.target.value);
                  }}
                />
              </div>

              <div className="col-12 text-center">
                <br />
                <button
                  className="btn btn-primary btn-sm"
                  onClick={() => {
                    async function makePay() {
                      let totalPrice =
                        parseInt(props.reprice.fare.totalFare.total.amount) +
                        parseInt(props.totalBaggage) +
                        parseInt(props.totalMeal);
                      let totalPax =
                        parseInt(props.reprice.searchQuery.paxCount.adt) +
                        parseInt(props.reprice.searchQuery.paxCount.chd) +
                        parseInt(props.reprice.searchQuery.paxCount.inf);
                      let totalWay = props.reprice.flights.length;

                      await props
                        .makePitakaMoPayment({
                          P_PARTNER_ID: "237677904183",
                          P_PARTNER_PIN: "!AA2728@",
                          P_PITAKAMO_ACCOUNT: props.kyc[0].KYC_MOBILE,
                          P_ACCOUNT_PIN: pin,
                          P_AMOUNT: totalPrice,
                          P_BAKARDA_SERVICE_CODE: props.results.isDomestic
                            ? 101
                            : 102,
                          P_PAX_COUNT: totalPax,
                          P_WAY_COUNT: totalWay,
                          P_CAR_COUNT: 0,
                          P_BUS_COUNT: 0,
                          P_ROOM_COUNT: 0,
                          P_NIGHT_COUNT: 0,
                          P_DAY_COUNT: 0,
                        })
                        .then((data) => {
                          if (data === true) {
                            async function bookFlight() {
                              await props.makeBooking(totalPrice);
                              history.push("/flight/finish");
                            }

                            bookFlight();
                          } else {
                            alert(data);
                          }
                        });
                    }

                    makePay();
                  }}
                >
                  Book Flight
                </button>
                <br />
                <br />
              </div>
            </div>
          </div>
        </div>
      );
    } catch(e) {

    }
  }

  return (
    <div>
      {layout()}
    </div>
  );
}

// export default Home;

const mapStateToProps = state => {
  return {
    token: state.token,
    reprice: state.reprice,
    passengersData: state.passengersData,
    search: state.search,
    totalMeal: state.totalMeal,
    totalBaggage: state.totalBaggage,
    kyc: state.kyc,
    fees: state.fees,
    results: state.results,
    flightKeys: state.flightKeys
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveToken: () => dispatch(saveToken()),
    saveResult: searchData => dispatch(saveResult(searchData)),
    saveKYC: id => dispatch(saveKYC(id)),
    calculateFee: params => dispatch(calculateFee(params)),
    makePitakaMoPayment: params => dispatch(makePitakaMoPayment(params)),
    makeBooking: params => dispatch(makeBooking(params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);