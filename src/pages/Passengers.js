import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";

import { connect } from "react-redux";
import { setPassengerData, co_url, saveKYC, repriceFlight } from "../redux/actions/actions";
import { useHistory } from "react-router-dom"
import moment from "moment";

import Flights from '../components/Flights';
import Select from "react-select";

function Passengers(props) {
  let history = useHistory();

  const [passengerDetailsModal, setPassengerDetailsModal] = useState(false);
  const [countryCodeModal, setCountryCodeModal] = useState(false);
  const [currentPType, setCurrentPType] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(null);

  const [countries, setCountries] = useState([]);

  const [title, setTitle] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [dateOfBirth, setDateOfBirth] = useState('');
  const [nationality, setNationality] = useState('');
  const [passportNumber, setPassportNumber] = useState('');
  const [passportIssue, setPassportIssue] = useState('');
  const [passportExpiry, setPassportExpiry] = useState('');
  const [passportIssuance, setPassportIssuance] = useState("");
  const [pitakamo, setPitakamo] = useState(null);
  const [phoneCode, setPhoneCode] = useState('63');
  const [searchCountry, setSearchCountry] = useState();

  const [passengerData, setPassengerData] = useState([]);
  const [currCountries, setCurrCountries] = useState([]);


  useEffect(() => {
    if (props.search === null) {
      history.push("/search/flight");
    }
  })

  useEffect(() => {
    let nC = countries.filter(v => {
      if(
        v.phonecode == searchCountry
     || v.name.includes(searchCountry)
     || v.code == searchCountry
      ) return true;

      return false;
    });

    setCurrCountries(nC)
  }, [searchCountry]);

  useEffect(() => {
    function compare( a, b ) {
      if ( a.name < b.name ){
        return -1;
      }
      if ( a.name > b.name ){
        return 1;
      }
      return 0;
    }

    axios({
      url : props.co_url() + "/api/countries",
      method: "get",
      headers: {
        'Authorization' : 'Bearer ' + props.token.access_token
      }
    }).then(response => {
      setCountries(Object.values(response.data).sort(compare));
      setCurrCountries(Object.values(response.data).sort(compare));
    })
  }, [])

  const travellers = () => {
    let t = [];
    let index = 0;
    for (let i = 1; i <= props.search.adtCount; i++) {
      t.push(
        <div className="card" data-key={index} key={index} onClick={(v) => {
          setCurrentPType('adt');
          setPassengerDetailsModal(true);
          let cIndex = v.currentTarget.getAttribute("data-key");
          setCurrentIndex(cIndex);
          
          let pd = passengerData[cIndex] || null;
          console.log(pd, ' --- checking pd')
          if(pd != null) {
            setTitle(pd.title)
            setFirstName(pd.firstName);
            setLastName(pd.lastName);
            setDateOfBirth(moment(pd.dob).format('YYYY-MM-DD'));
            if(pd.passport != null) {
              setPassportNumber(pd.passport.num);
              setNationality(pd.passport.nat);

              // setter
              // var opts = document.getElementById('nationality').options;
              // for (var opt, j = 0; opt = opts[j]; j++) {
              //   if (opt.value == pd.passport.nat) {
              //     console.log(opt.value  + ' ' + pd.passport.nat, '----- gago!!!!!!!!!!!');
              //     document.getElementById("nationality").selectedIndex = j;
              //     break;
              //   }
              // }

              setPassportExpiry(moment(pd.passport.doe).format('YYYY-MM-DD'));
              setPassportIssuance(moment(pd.passport.doi).format('YYYY-MM-DD'));
            }
          }

        }}>
          <div className="card-body">
            <div className="row">
              <div className="col-1">
                <i className="fa fa-fw fa-user"></i>
              </div>
              <div className="col-10">
                {passengerData[index] == undefined ? 'Adult ' + i : passengerData[index].title + ' ' + passengerData[index].firstName + ' ' + passengerData[index].lastName}
              </div>
            </div>
          </div>
        </div>
      );

      index++;
    }

    for (let i = 1; i <= props.search.chdCount; i++) {
      t.push(
        <div className="card" data-key={index} key={index} onClick={(v) => {
          setCurrentPType("chd");
          setPassengerDetailsModal(true);
          setCurrentIndex(v.currentTarget.getAttribute("data-key"));
        }}>
          <div className="card-body">
            <div className="row">
              <div className="col-1">
                <i className="fa fa-fw fa-user"></i>
              </div>
              <div className="col-10">
                {passengerData[index] == undefined ? 'Children ' + i : passengerData[index].title + ' ' + passengerData[index].firstName + ' ' + passengerData[index].lastName}
              </div>
            </div>
          </div>
        </div>
      );
      index++;
    }

    for (let i = 1; i <= props.search.infCount; i++) {
      t.push(
        <div className="card" data-key={index} key={index} onClick={(v) => {
          setCurrentPType("inf");
          setPassengerDetailsModal(true);
          setCurrentIndex(v.currentTarget.getAttribute("data-key"));
        }}>
          <div className="card-body">
            <div className="row">
              <div className="col-1">
                <i className="fa fa-fw fa-user"></i>
              </div>
              <div className="col-10">
                {passengerData[index] == undefined ? 'Infant ' + i : passengerData[index].title + ' ' + passengerData[index].firstName + ' ' + passengerData[index].lastName}
              </div>
            </div>
          </div>
        </div>
      );
      index++;
    }
    
    return t;
  }

  const layout = () => {
    try {
      return (
        <div>
          <div className="header">
            <div className="container-fluid search-result-holder">
              <div className="row">
                <div className="col-5">
                  <h1 className="title-2x m-0 text-white search-code">
                    {props.search.fromCode}
                  </h1>
                  <small className="text-header-darker m-0 search-description">
                    {props.search.fromDescription}
                  </small>
                </div>
                <div className="col-2">
                  <br />
                  <i className="fa fa-2x fa-fw fa-plane text-header-darker"></i>
                </div>
                <div className="col-5">
                  <h1 className="title-2x m-0 text-white search-code">
                    {props.search.toCode}
                  </h1>
                  <small className="text-header-darker m-0 search-description">
                    {props.search.toDescription}
                  </small>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-7">
                  <h1 className="title text-white">
                    <div>
                      {props.search.adtCount}Adult, {props.search.chdCount}Child,{" "}
                      {props.search.infCount}Infant
                    </div>
                    <small className="text-header-darker">Passengers</small>
                  </h1>
                </div>
                <div className="col-5">
                  <h1 className="title text-white">
                    <div>{props.search.seatClass}</div>
                    <small className="text-header-darker">Type of Class</small>
                  </h1>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-12 text-center">
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={() => history.push("/search/flight")}
                  >
                    <i className="fa fa-search fa-fw"> </i> Change Search
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-12">
                <h1 className="title">PitakaMo Account</h1>
                <div className="alert alert-info small">
                  We'll be sending the flight ticket to your PitakaMo Registered
                  Email Address
                </div>
                <div className="form-group">
                  <label htmlFor="" style={{ margin: "0px" }}>
                    Mobile Number
                  </label>
                  <div className="row search-holder">
                    <div className="col-4 suggestion-container">

                      <input
                        type="text"
                        className="form-control"
                        value={phoneCode}
                        readOnly
                        onClick={() => {
                          setCountryCodeModal(true);
                        }}
                      />
                    </div>
                    <div className="col-8">
                      <input
                        type="number"
                        className="form-control"
                        pattern="[0-9]*"
                        max="10"
                        min="10"
                        placeholder="Mobile Number"
                        onChange={v => {
                          setPitakamo(v.target.value);
                        }}
                      />
                      <small>Without Area Code</small>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12">
                <h1 className="title">Travellers</h1>
                {travellers()}
              </div>

              <div className="col-12 text-center">
                <button
                  className="btn btn-primary btn-sm"
                  onClick={async () => {
                    let totalPassenger =
                      parseInt(props.search.adtCount) +
                      parseInt(props.search.chdCount) +
                      parseInt(props.search.infCount);

                    // await props.saveKYC(phoneCode + "" + pitakamo).then(e => {
                    //   if (!e) {
                    //     alert("Invalid PitakaMo Mobile Number");
                    //     return false;
                    //   } else {
                    //     passengerData.map(v => {
                    //       if (v == null) {
                    //         alert("All Travellers Data are Required");
                    //       }
                    //     });
                    //     if (passengerData.length == totalPassenger) {
                          props.setPassengerData(passengerData);
                          props.repriceFlight().then(d => {
                            history.push("/flight/addon");
                          });
                      //   } else {
                      //     alert("All Travellers Data are Required");
                      //   }
                      // }
                    // });
                  }}
                >
                  Continue <i className="fa fa-fw fa-chevron-right"> </i>
                </button>
              </div>
            </div>
          </div>
          <br />
          <br />

          <div className={"bt-modal " + (countryCodeModal ? "visible" : "")}>
            <div className="bt-modal-body">
              <div className="container">
                <div className="row">
                  <div className="col-12">
                    <h1 className="title float-left">Area Code</h1>
                    <i
                      className="fa fa-fw fa-lg fa-close float-right"
                      style={{ marginTop: "8px" }}
                      onClick={() => setCountryCodeModal(false)}
                    ></i>
                    <div className="clearfix"></div>
                  </div>

                  <div className="col-12 form-group">
                    <input
                      type="text"
                      placeholder="Search Country"
                      className="form-control"
                      value={searchCountry}
                      onChange={
                        (v) => {
                          setSearchCountry(v.target.value);
                        }
                      }
                    />
                  </div>

                  <div className="col-12">
                    <ul className="list-group list-group-flush">
                      {
                        currCountries.map((v, i) => {
                          return (
                            <li key={i} className="list-group-item" style={{'background': 'transparent'}} onClick={
                              () => {
                                setCountryCodeModal(false)
                                setPhoneCode(v.phonecode)
                              }
                            }>
                              {/* <img src={require('../assets/img/countries/'+ (v.code).toLowerCase() +'.png')} className="float-left" alt="" width="30" srcset=""/> */}
                              
                              <div className="float-left">
                                {
                                  phoneCode != v.phonecode ? 
                                  (<i style={{'marginTop' : '6px', 'marginLeft': '-5px', 'color' : '#cacaca'}} className="fa fa-fw fa-circle"></i>) :
                                  (<i style={{'marginTop' : '6px', 'marginLeft': '-5px', 'color' : '#68d855'}} className="fa fa-fw fa-circle"></i>)
                                }
                                
                              </div>
                              
                              <div className="float-left">
                                <small><strong>{ v.phonecode }</strong> { v.name }</small>
                              </div>

                              
                              <div className="clearfix"></div>
                            </li>
                          );
                        })
                      }
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div
            className={"bt-modal " + (passengerDetailsModal ? "visible" : "")}
          >
            <div className="bt-modal-body">
              <div className="container">
                <div className="col-12">
                  <h1 className="title float-left">Traveler Details</h1>
                  <i
                    className="fa fa-fw fa-lg fa-close float-right"
                    style={{ marginTop: "8px" }}
                    onClick={() => setPassengerDetailsModal(false)}
                  ></i>
                  <div className="clearfix"></div>
                </div>

                <div className="col-12">
                  <div className="alert alert-warning small">
                    Please make sure that the given data is the same with the ID
                    you'll show to the Airport.
                    {props.reprice.cndtns.passport.applicable ? (
                      <div>
                        <br />
                        Please make sure your passport expiry date is not less
                        than 6 months
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
                <div className="col-12 form-group">
                  <label htmlFor="">Title</label>
                  <select
                    className="form-control"
                    value={title}
                    onChange={v => setTitle(v.target.value)}
                  >
                    <option value="-1">Select Title</option>
                    {props.reprice.titles[currentPType] !== undefined
                      ? props.reprice.titles[currentPType].map(v => {
                          return (
                            <option
                              value={v}
                              selected={title == v ? "true" : "false"}
                            >
                              {v}
                            </option>
                          );
                        })
                      : ""}
                  </select>
                </div>
                <div className="col-12 form-group">
                  <label htmlFor="">First Name</label>
                  <input
                    type="text"
                    className="form-control"
                    value={firstName}
                    onChange={v => setFirstName(v.target.value)}
                  />
                </div>
                <div className="col-12 form-group">
                  <label htmlFor="">Last Name</label>
                  <input
                    type="text"
                    className="form-control"
                    value={lastName}
                    onChange={v => setLastName(v.target.value)}
                  />
                </div>

                <div className="col-12 form-group">
                  <label htmlFor="">Date of Birth</label>
                  <input
                    type="date"
                    className="form-control"
                    value={dateOfBirth}
                    onChange={v => setDateOfBirth(v.target.value)}
                  />
                </div>

                {props.reprice.cndtns.passport.applicable ? (
                  <div>
                    <div className="col-12 form-group">
                      <label htmlFor="">Nationality</label>

                      <select
                        className="form-control"
                        value={nationality}
                        id="nationality"
                        onChange={v => {
                          console.log(v.target)
                          console.log(v.currentTarget.value)
                          setNationality(v.currentTarget.value);
                        }}
                      >
                        <option value="-1">Select Country</option>
                        {countries.map(v => {
                          return (
                            <option
                              value={v.code}
                              selected={
                                v.code == nationality ? true : false
                              }
                            >
                              {v.name}
                            </option>
                          );
                        })}
                      </select>
                    </div>

                    <div className="col-12 form-group">
                      <label htmlFor="">Passport Number</label>
                      <input
                        type="text"
                        className="form-control"
                        value={passportNumber}
                        onChange={v => setPassportNumber(v.target.value)}
                      />
                    </div>

                    <div className="col-12 form-group">
                      <label htmlFor="">Passport Issuance Date</label>
                      <input
                        type="date"
                        className="form-control"
                        value={passportIssuance}
                        onChange={v => setPassportIssuance(v.target.value)}
                      />
                    </div>

                    <div className="col-12 form-group">
                      <label htmlFor="">Passport Expiry Date</label>
                      <input
                        type="date"
                        className="form-control"
                        value={passportExpiry}
                        onChange={v => setPassportExpiry(v.target.value)}
                      />
                    </div>
                  </div>
                ) : (
                  ""
                )}

                <div className="col-12 text-center">
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={() => {
                      if (
                        title == "" ||
                        firstName == "" ||
                        lastName == "" ||
                        dateOfBirth == "" ||
                        props.kyc != null
                      )
                        return false;

                      let pdata = {
                        title: title,
                        firstName: firstName,
                        lastName: lastName,
                        pType: currentPType,
                        dob: moment(dateOfBirth).format("YYYY-M-D")
                      };

                      if (props.reprice.cndtns.passport.applicable) {
                        pdata["passport"] = {
                          nat: nationality,
                          num: passportNumber,
                          doe: moment(passportExpiry).format("YYYY-M-D"),
                          doi: moment(passportIssuance).format("YYYY-M-D")
                        };
                      }

                      console.log(currentIndex, " current index save");
                      let updatePassengerData = passengerData;
                      updatePassengerData[currentIndex] = pdata;
                      console.log(updatePassengerData, "update passenger data");
                      setTitle("");
                      setFirstName("");
                      setLastName("");
                      setDateOfBirth("");
                      setNationality("");
                      setPassportNumber("");
                      setPassportIssue("");
                      setPassportExpiry("");
                      setPassportIssuance("");
                      setPassengerData(updatePassengerData);
                      setPassengerDetailsModal(false);
                    }}
                  >
                    <i className="fa fa-fw fa-chevron-right"> </i> Save
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } catch(e) {

    }
  }

  return (
    <div>
      {layout()}
    </div>
  );
}

// export default Home;

const mapStateToProps = state => {
  return {
    token: state.token,
    results: state.results,
    search: state.search,
    onwardFlight: state.onwardFlight,
    returnFlight: state.returnFlight,
    currentResults: state.currentResults,
    reprice: state.reprice,
    kyc: state.kyc
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setPassengerData: passengersData => dispatch(setPassengerData(passengersData)),
    co_url: () => {return co_url},
    saveKYC: id => dispatch(saveKYC(id)),
    repriceFlight: () => dispatch(repriceFlight())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Passengers);
