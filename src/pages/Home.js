import React, {useState, useEffect} from "react";
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import axios from "axios";

import { connect } from "react-redux";
import { saveToken, saveResult, saveKYC } from "../redux/actions/actions";

import { useHistory, useParams } from "react-router-dom";

import moment from 'moment'


function Home(props) {
  let history = useHistory();
  let { id } = useParams();

    const [test, setTest] = useState(null);

    // useEffect(() => {
    //   async function anyNameFunction() {
    //     await props.saveToken();
    //     setTest(true);
    //   }
    //   anyNameFunction();
    // }, []);

    useEffect(() => {
      history.push("/search/flight");
    }, [])
    
    return (
      <div>
	</div>
    );
}

// export default Home;

const mapStateToProps = state => {
  return {
    token: state.token
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveToken: () => dispatch(saveToken()),
    saveResult: searchData => dispatch(saveResult(searchData)),
    saveKYC: id => dispatch(saveKYC(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);